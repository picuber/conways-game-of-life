import os
import time


class Board:
    """The playing board for Conway's Game of Life"""

    def __init__(self, width, height):
        """
        :fields: (bool[][]) 2D array of fields for the board; True = alive
            fields[y][x]
            fields[height][width]
        """
        self.width = width
        self.height = height
        self.fields = [[False for _ in range(width)] for _ in range(height)]
        self.evh = EventHandler(self)
        self.print_coord = False

    def __repr__(self):
        out = ''
        if self.print_coord:
            out += '\\'
            for i in range(self.width):
                out += '{} '.format(i % 10)
            out += '\n'
        for y in range(self.height):
            if self.print_coord:
                out += str(y % 10)
            for x in range(self.width):
                out += '⬛' if self.fields[y][x] else '⬜'
            out += '\n'
        return out

    def get(self, x, y):
        if (not 0 <= x < self.width) or (not 0 <= y < self.height):
            raise ValueError
        return self.fields[y][x]

    def set(self, x, y, val):
        if (not 0 <= x < self.width) or (not 0 <= y < self.height):
            print('[E] Index out of bounds')
            raise ValueError
        if not isinstance(val, bool):
            raise ValueError
        self.fields[y][x] = val

    def step(self):
        # make copy for next step to progress each field simultaneously
        new = [row[:] for row in self.fields]
        for y in range(self.height):
            for x in range(self.width):
                n = self._alive_neighbours(x, y)
                if self.fields[y][x]:
                    new[y][x] = 2 <= n <= 3
                else:
                    new[y][x] = n == 3
        self.fields = new

    def _alive_neighbours(self, x, y):
        """
        :x, y: (int)
        :returns: (int) the number of alive neighbours

        """
        count = 0
        for x_ in range(max(0, x-1), min(self.width, x+2)):
            for y_ in range(max(0, y-1), min(self.height, y+2)):
                if (x, y) != (x_, y_) and self.fields[y_][x_]:
                    count += 1
        return count

    def event_loop(self):
        clear()
        print(self)
        while True:
            try:
                self.evh.handle(input('>> '))
            except KeyboardInterrupt:
                pass
            except EOFError:
                break

    def to_file_format(self):
        out = '{},{}\n'.format(self.width, self.height)
        for y in range(self.height):
            for x in range(self.width):
                out += '1' if self.fields[y][x] else '0'
            out += '\n'
        return out

    @staticmethod
    def parse_file_format(in_lines):
        """
        :in_lines: (str[])
        The file format is as follows:
            - first line: <width>, <height>
            - following lines: series of '1's (alive) and '0's (dead)
              any other character is omitted as comment or stylistic formatting
              each line represents one row in the board
              rows of insufficient length will implicitly be padded with '0's
              in each row only <width> valid characters will be processed
              missing rows will be interpreted as all '0' rows
        :returns: either a board with the structure from the input
            or None if there was a parsing error
        """
        dim = in_lines[0].split(',')
        in_lines = in_lines[1:]
        try:
            board = Board(int(dim[0]), int(dim[1]))
            for y in range(len(in_lines)):
                x_f = 0
                for x_c in range(len(in_lines[y])):
                    if in_lines[y][x_c] not in ['1', '0']:
                        continue
                    board.fields[y][x_f] = in_lines[y][x_c] == '1'
                    x_f += 1
                    if x_f >= board.width:
                        break
            return board
        except Exception as e:
            raise e
            return None

    def resize(self, width, height):
        """
        resize the board while keeping the origin (top left) in place
        new areas will be filled with dead cells
        """
        if width <= 0 or height <= 0:
            raise ValueError

        # adjust width
        if width < self.width:
            for y in range(self.height):
                del self.fields[y][(width - self.width):]
        elif width > self.width:
            for y in range(self.height):
                self.fields[y].extend([False] * (width - self.width))

        # adjust height
        if height < self.height:
            del self.fields[(height - self.height):]
        elif height > self.height:
            self.fields.extend([[False for _ in range(width)]
                                for _ in range(height - self.height)])

        self.width = width
        self.height = height

    @staticmethod
    def merge(this, that, x, y):
        """
        merge that into this where origin of that is at (x, y) in this
        """
        if x < 0 or y < 0:
            raise ValueError
        if (x + that.width >= this.width) or (y + that.height >= this.height):
            this.resize(x + that.width, y + that.height)

        for y_ in range(that.height):
            for x_ in range(that.width):
                this.fields[y + y_][x + x_] = that.fields[y_][x_]

        return this


class EventHandler:
    """Handles the input commands/events for the Board"""

    def __init__(self, board):
        self.board = board
        self.run_delay = 0.5
        self.step_count = 0

    def _nop(self, args):
        clear()
        print(self.board)

    def _help(self, args):
        """
        :args: ignored
        """
        clear()
        print('-----HELP-----\n'
              'help/h/?:                Show this message\n'
              'step/s:                  Execute one/n time step(s) instantly\n'
              'run/r:                   Run (n) steps with a delay\n'
              'delay/d:                 Set the delay between steps with run\n'
              'put/p/birth/b:           Put the cell at position (x, y)\n'
              'remove/rem/k/kill/die:   Remove the cell at position (x, y)\n'
              'flip/f                   Flip the cell at position (x, y)\n'
              'quit/q/exit/x            Exit Conway\'s Game of Life\n'
              'save                     Save the board to a file\n'
              'menu/m                   Return to main menu\n'
              'insert/i/merge           Open Merge Menu\n'
              'toggle/t                 Toggle the coornidate numbers on the '
              'border\n'
              )

    def _step(self, args):
        """
        :args: if exists and is int, do args[0] steps instatntly instead of one
        """
        num = 1
        if len(args) > 0:
            try:
                num = int(args[0])
            except ValueError:
                pass
        for _ in range(num):
            self.board.step()
            self.step_count += 1
        clear()
        print(self.board)
        print('[I] Step {}'.format(self.step_count))

    def _run(self, args):
        """
        :args: if exists and is int, do only up to args[0] steps
        """

        def test(inf):
            return True if inf else count < n

        inf = True
        count = 0
        if len(args) > 0:
            try:
                n = int(args[0])
                inf = False
            except ValueError:
                pass

        try:
            while test(inf):
                count += 1
                self._step([])
                print('[I] To interrupt run hit ctrl+c')
                time.sleep(self.run_delay)
        except KeyboardInterrupt:
            pass

    def _set_delay(self, args):
        clear()
        print(self.board)
        if len(args) > 0:
            try:
                self.run_delay = float(args[0])
                print('[I] Set delay to {}'.format(self.run_delay))
                return
            except ValueError:
                pass
        print('[E] Usage: delay <time: float>')

    def _put(self, args):
        clear()
        if len(args) >= 2:
            try:
                x = int(args[0])
                y = int(args[1])
                self.board.set(x, y, True)
                print(self.board)
                print('[I] Put cell at ({}, {})'.format(x, y))
                return
            except ValueError:
                pass
        print(self.board)
        print('[E] Usage: put <x: [0..{}]> <y: [0..{}]>'
              .format(self.board.width, self.board.height))

    def _remove(self, args):
        clear()
        if len(args) >= 2:
            try:
                x = int(args[0])
                y = int(args[1])
                self.board.set(x, y, False)
                print(self.board)
                print('[I] Removed cell at ({}, {})'.format(x, y))
                return
            except ValueError:
                pass
        print(self.board)
        print('[E] Usage: remove <x: [0..{}]> <y: [0..{}]>'
              .format(self.board.width, self.board.height))

    def _flip(self, args):
        clear()
        if len(args) >= 2:
            try:
                x = int(args[0])
                y = int(args[1])
                val = not self.board.get(x, y)
                self.board.set(x, y, val)
                print(self.board)
                print('[I] Flipped cell at ({}, {})'.format(x, y))
                return
            except ValueError:
                pass
        print(self.board)
        print('[E] Usage: flip <x: [0..{}]> <y: [0..{}]>'
              .format(self.board.width, self.board.height))

    def _quit(self, args):
        exit()

    def _save(self, args):
        """
        :args: ignored
        """
        Menus.save_menu(self.board)

    def _menu(self, args):
        answer = input('Unsaved progress will be lost. Continue?\n[y/N]>> ')
        if answer != 'y':
            return
        raise EOFError  # breaks out of event handler

    def _resize(self, args):
        clear()
        if len(args) >= 2:
            try:
                w = int(args[0])
                h = int(args[1])
                self.board.resize(w, h)
                print(self.board)
                print('[I] Resized board to {}x{}'.format(w, h))
                return
            except ValueError:
                pass
        print(self.board)
        print('[E] Usage: resize <width: int> <heigth: int>')

    def _merge(self, args):
        Menus.merge_menu(self.board)

    def _toggle(self, args):
        self.board.print_coord = not self.board.print_coord
        clear()
        print(self.board)

    def _default(self, args):
        clear()
        print(self.board)
        print('[E] unknown command\n'
              '[I] use "help" for a list of commands')

    def handle(self, cmd):
        switcher = {
            '':         self._nop,
            '?':        self._help,
            'h':        self._help,
            'help':     self._help,
            's':        self._step,
            'step':     self._step,
            'r':        self._run,
            'run':      self._run,
            'd':        self._set_delay,
            'delay':    self._set_delay,
            'p':        self._put,
            'put':      self._put,
            'b':        self._put,
            'birth':    self._put,
            'k':        self._remove,
            'kill':     self._remove,
            'die':      self._remove,
            'rem':      self._remove,
            'remove':   self._remove,
            'f':        self._flip,
            'flip':     self._flip,
            'q':        self._quit,
            'quit':     self._quit,
            'x':        self._quit,
            'exit':     self._quit,
            'save':     self._save,
            'm':        self._menu,
            'menu':     self._menu,
            'res':      self._resize,
            'resize':   self._resize,
            'i':        self._merge,
            'insert':   self._merge,
            'merge':    self._merge,
            't':        self._toggle,
            'toggle':   self._toggle,
        }

        cmd = cmd.strip().split(' ')
        switcher.get(cmd[0], self._default)(cmd[1:])


class Menus:
    board = None

    @classmethod
    def _run_board(cls):
        while True:
            try:
                cls.board.event_loop()
            except Rerun:
                pass
            except ToMainMenu:
                break

    @staticmethod
    def _menu_print(menu):
        for key, (text, _) in menu.items():
            print(text)

    @staticmethod
    def _create_menu():
        width, height = Menus._get_2_nums('',
                                          'Please enter the size\n'
                                          'width x height >> ',
                                          'x')
        return Board(width, height)

    @staticmethod
    def _load_menu():
        clear()
        while True:
            try:
                path = input('Please enter the file name\n>> ').strip()
            except (KeyboardInterrupt, EOFError):
                return None
            if os.path.isfile(path):
                break
            clear()
            print('[E] File does not exist. Please re-enter the file name\n'
                  '[I] To abort hit ctrl+c')

        with open(path) as f:
            return Board.parse_file_format([line.strip() for line in f])

    @staticmethod
    def save_menu(board):
        clear()
        print(board)
        while True:
            try:
                path = input('Please enter a file name for saving\n>> ')
            except (KeyboardInterrupt, EOFError):
                clear()
                print(board)
                print('\n[I] Did *not* save board')
                return
            if not path:
                clear()
                print(board)
                continue
            if os.path.exists(path):
                clear()
                print(board)
                print('[E] File already exists. '
                      'Please enter another file name\n'
                      '[I] To abort hit ctrl+c')
                continue
            break

        with open(path, 'x') as f:
            f.write(board.to_file_format())
        clear()
        print(board)
        print('[I] Board saved.')

    @classmethod
    def main_menu(cls):
        clear()

        def default():
            clear()
            cls._menu_print(menu)
            print('\n[E] Please enter the corresponding number in the bracket')

        menu = {
            '1': ('[1] Create new board', cls._create_menu),
            '2': ('[2] Load exististing board from file', cls._load_menu),
            '3': ('[3] Exit', exit)
        }

        while True:
            clear()
            cls._menu_print(menu)
            choice = input('>> ').strip()
            board = menu.get(choice, ('', default))[1]()
            if board:
                cls.board = board
                cls._run_board()

    @staticmethod
    def _get_2_nums(text, prompt, sep):
        clear()
        while True:
            try:
                if text:
                    print(text)
                nums = input(prompt).strip().split(sep)
                a, b = int(nums[0]), int(nums[1])
            except ValueError:
                clear()
                print('[E] Please enter two ints like this: a{}b'.format(sep))
                continue
            except (KeyboardInterrupt, EOFError):
                return None
            return a, b

    @classmethod
    def _do_merge(cls, board, direction):
        """
        merge the current board into another board (direction = False)
        or merge another board into the current one (direction = True)
        :returns: the merged board

        """
        clear()

        def nop():
            return ...

        def default():
            clear()
            cls._menu_print(menu)
            print('\n[E] Please enter the corresponding number in the bracket')

        menu = {
            '1': ('[1] Create new board', cls._create_menu),
            '2': ('[2] Load exististing board from file', cls._load_menu),
        }

        while True:
            clear()
            cls._menu_print(menu)
            choice = input('>> ').strip()
            other = menu.get(choice, ('', default))[1]()
            if other:
                break

        x, y = cls._get_2_nums('In what position do you want to merge?',
                               'Please enter the position\nx, y >> ',
                               ',')
        if direction:
            return Board.merge(board, other, x, y)
        else:
            return Board.merge(other, board, x, y)

    @classmethod
    def _merge_into(cls, board):
        return cls._do_merge(board, False)

    @classmethod
    def _merge_with(cls, board):
        return cls._do_merge(board, True)

    @classmethod
    def merge_menu(cls, board):
        clear()

        def nop(board):
            return board

        def default():
            clear()
            cls._menu_print(menu)
            print('\n[E] Please enter the corresponding number in the bracket')

        menu = {
            '1': ('[1] Merge this board into another board', cls._merge_into),
            '2': ('[2] Merge another board into this board', cls._merge_with),
            '3': ('[3] Cancel', nop)
        }

        while True:
            clear()
            cls._menu_print(menu)
            choice = input('>> ').strip()
            try:
                board = menu.get(choice, ('', default))[1](board)
            except KeyboardInterrupt:
                continue
            if board:
                cls.board = board
                raise Rerun


class Rerun(Exception):
    """
    signals the Menu._run_board() method to run the event loop of the new board
    """
    pass


class ToMainMenu(Exception):
    """
    signals the Menu._run_board() method to break out of the loop and return
    to the main menu
    """


def clear():
    os.system('cls' if os.name == 'nt' else 'clear')


def main():
    Menus().main_menu()


if __name__ == "__main__":
    main()
